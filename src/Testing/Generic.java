package Testing;

public class Generic<E> {

    Generic<E> next;
    E data;

    public Generic() {
        this.data = null;
        this.next = null;
    }

    public Generic(E data){
        this.data = data;
    }

    public Generic(E data, Generic<E> prev){
        this(data);
        this.next = prev.next;
        prev.next = this;
    }

    public static void main(String[] args) {
        Generic<Integer> n0 = new Generic<>();
        Generic<Integer> n1 = new Generic<>(1, n0);
        Generic<Integer> n2 = new Generic<>(2, n0);
        while (n0 != null)n0 = n0.next;
    }
}

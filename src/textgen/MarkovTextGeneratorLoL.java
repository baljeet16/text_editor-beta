package textgen;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/** 
 * An implementation of the MTG interface that uses a list of lists.
 * @author UC San Diego Intermediate Programming MOOC team 
 */
public class MarkovTextGeneratorLoL implements MarkovTextGenerator {

	// The list of words with their next words
	private List<ListNode> wordList; 
	
	// The starting "word"
	private String starter;
	
	// The random number generator
	private Random rnGenerator;
	
	public MarkovTextGeneratorLoL(Random generator)
	{
		wordList = new LinkedList<ListNode>();
		starter = "";
		rnGenerator = generator;
	}
	
	public int getIndex(String word){
        int cnt = 0;
        for(ListNode node:wordList){
            if (node.getWord().equals(word))return cnt;
            ++cnt;
        }
        return -1;
    }

	/** Train the generator by adding the sourceText */
	@Override
	public void train(String sourceText)
	{
		// TODO: Implement this method
		String []words = sourceText.split("[ ]+");

        if (words == null || words.length == 0 || sourceText.length() == 0 || sourceText.split("[a-zA-Z]+").length == 0)
            return;

        starter = words[0];
        String pre = starter.trim();
        for (int i = 1; i <= words.length; i++) {
            String cur = "";
            if (i == words.length)cur = starter.trim();
            else cur = words[i].trim();
            int index = getIndex(pre);
            if (index != -1){
                wordList.get(index).addNextWord(cur);
            }
            else {
                wordList.add(new ListNode(pre));
                wordList.get(wordList.size() - 1).addNextWord(cur);
            }
            if (i == words.length)break;
            pre = words[i];
        }
    }
	
	/** 
	 * Generate the number of words requested.
	 */
	@Override
	public String generateText(int numWords) {
	    // TODO: Implement this method
        if (numWords < 0)throw new NumberFormatException("Number of words can't be negative");
        if (numWords == 0 || wordList == null || wordList.size() == 0)return "";

        String curWord = starter, output = starter;
        while (numWords-- > 1){
            int index = getIndex(curWord);
            String generatedWord = wordList.get(index).getRandomNextWord(rnGenerator);
            output += " "+     generatedWord;
            curWord = generatedWord;
        }
        return output;

	}
	
	
	// Can be helpful for debugging
	@Override
	public String toString()
	{
		String toReturn = "";
		for (ListNode n : wordList)
		{
			toReturn += n.toString();
		}
		return toReturn;
	}
	
	/** Retrain the generator from scratch on the source text */
	@Override
	public void retrain(String sourceText)
	{
		// TODO: Implement this method.
        wordList.clear();
        this.starter = "";
        train(sourceText);
	}
	
	// TODO: Add any private helper methods you need here.
	
	
	/**
	 * This is a minimal set of tests.  Note that it can be difficult
	 * to test methods/classes with randomized behavior.   
	 * @param args
	 */
	public static void main(String[] args)
	{
		// feed the generator a fixed random value for repeatable behavior
//        BigInteger primeGenerator = BigInteger.probablePrime(60,new Random());
        MarkovTextGeneratorLoL gen = new MarkovTextGeneratorLoL(new Random(655309968241573963L));
//        System.out.println(primeGenerator+"\n\n");
        String textString = "Hello.  Hello there.  This is a test.  Hello there.  Hello Bob.  Test again.";
//		System.out.println(textString);
		gen.train(textString);
        System.out.println(gen.generateText(20));
//        System.out.println(gen);
        gen.train("");
        System.out.println(gen.generateText(20));
		String textString2 = "You say yes, I say no, "+
				"You say stop, and I say go, go, go, "+
				"Oh no. You say goodbye and I say hello, hello, hello, "+
				"I don't know why you say goodbye, I say hello, hello, hello, "+
				"I don't know why you say goodbye, I say hello. "+
				"I say high, you say low, "+
				"You say why, and I say I don't know. "+
				"Oh no. "+
				"You say goodbye and I say hello, hello, hello. "+
				"I don't know why you say goodbye, I say hello, hello, hello, "+
				"I don't know why you say goodbye, I say hello. "+
				"Why, why, why, why, why, why, "+
				"Do you say goodbye. "+
				"Oh no. "+
				"You say goodbye and I say hello, hello, hello. "+
				"I don't know why you say goodbye, I say hello, hello, hello, "+
				"I don't know why you say goodbye, I say hello. "+
				"You say yes, I say no, "+
				"You say stop and I say go, go, go. "+
				"Oh, oh no. "+
				"You say goodbye and I say hello, hello, hello. "+
				"I don't know why you say goodbye, I say hello, hello, hello, "+
				"I don't know why you say goodbye, I say hello, hello, hello, "+
				"I don't know why you say goodbye, I say hello, hello, hello,";
//		System.out.println(textString2);
		gen.retrain(textString2);
//		System.out.println(gen);
		System.out.println(gen.generateText(20));
	}

}

/** Links a word to the next words in the list 
 * You should use this class in your implementation. */
class ListNode
{
    // The word that is linking to the next words
	private String word;
	
	// The next words that could follow it
	private List<String> nextWords;
	
	ListNode(String word)
	{
		this.word = word;
		nextWords = new LinkedList<String>();
	}
	
	public String getWord()
	{
		return word;
	}

	public void addNextWord(String nextWord)
	{
		nextWords.add(nextWord);
	}
	
	public String getRandomNextWord(Random generator)
	{
		// TODO: Implement this method
	    // The random number generator should be passed from 
	    // the MarkovTextGeneratorLoL class
	    return nextWords.get(generator.nextInt(nextWords.size()));
	}

	public String toString()
	{
		String toReturn = word + ": ";
		for (String s : nextWords) {
			toReturn += s + "->";
		}
		toReturn += "\n";
		return toReturn;
	}
	
}



package textgen;

import java.util.AbstractList;


/** A class that implements a doubly linked list
 * 
 * @author UC San Diego Intermediate Programming MOOC team
 *
 * @param <E> The type of the elements stored in the list
 */
public class MyLinkedList<E> extends AbstractList<E> {
	LLNode<E> head;
	LLNode<E> tail;
	int size;

	/** Create a new empty LinkedList */
	public MyLinkedList() {
		// TODO: Implement this method
		this.head = null;
        this.tail = null;
        this.size = 0;
	}

	/**
	 * Appends an element to the end of the list
	 * @param element The element to add
	 */
	public boolean add(E element )
	{
		// TODO: Implement this method
        if (element == null)throw new NullPointerException("Data should not be null");

        if (this.head == null){
            this.head = this.tail = new LLNode<>(element);
            this.size = 1;
            return true;
        }
        LLNode<E> temp = new LLNode<>(element);
        this.tail.next = temp;
        temp.prev = this.tail;
        this.tail = temp;
        this.size++;
		return true;
	}

	/** Get the element at position index 
	 * @throws IndexOutOfBoundsException if the index is out of bounds. */
	public E get(int index) 
	{
		// TODO: Implement this method.
        if (index < 0 || index >= size())throw new IndexOutOfBoundsException("index is not in list range");

        LLNode<E> trav = this.head;
        while (trav != null && index > 0){
            trav = trav.next;
            index--;
        }
		return trav.data;
	}

	/**
	 * Add an element to the list at the specified index
	 * @param The index where the element should be added
	 * @param element The element to add
	 */
	public void add(int index, E element ) 
	{
		// TODO: Implement this method
        if (element == null)throw new NullPointerException("Data should not be null");

        if (index < 0 || index > size())throw new IndexOutOfBoundsException("index are out of List region");

        //If it is the first element of the list
        if (index == 0){
            if (this.size == 0){
                this.head = this.tail = new LLNode<>(element);
                this.size++;
                return;
            }
            this.size++;
            LLNode<E> temp = this.head;
            this.head = new LLNode<>(element);
            this.head.next = temp;
            temp.prev = this.head;
            return;
        }

        //If it is the last element of the list
        if (index == size){
            add(element);
            return;
        }

        LLNode<E> prev = head, cur = head;
        while (index-- > 0){
            prev = cur;
            cur = cur.next;
        }
        LLNode<E> add = new LLNode<>(element);
        prev.next = add;
        cur.prev = add;
        add.prev = prev;
        add.next = cur;
        this.size++;
	}


	/** Return the size of the list */
	public int size() 
	{
		// TODO: Implement this method
		return this.size;
	}

	/** Remove a node at the specified index and return its data element.
	 * @param index The index of the element to remove
	 * @return The data element removed
	 * @throws IndexOutOfBoundsException If index is outside the bounds of the list
	 * 
	 */
	public E remove(int index) 
	{
		// TODO: Implement this method

        if (index < 0 || index >= size())throw new IndexOutOfBoundsException("index are out of range");

        E data = get(index);
        LLNode<E> prev = this.head, cur = this.head;
        while (index-- > 0){
            prev = cur;
            cur = cur.next;
        }
        if (this.size() == 1){
            this.head = this.tail = null;
            this.size--;
            return data;
        }
        if (cur == this.head){
            this.head = cur.next;
            cur.prev = null;
            this.size--;
            return data;
        }
        if (cur == this.tail){
            this.tail = cur.prev;
            this.tail.next = null;
            this.size--;
            return data;
        }
        cur = cur.next;
        prev.next = cur;
        cur.prev = prev;
        this.size--;
        return data;
	}

	/**
	 * Set an index position in the list to a new element
	 * @param index The index of the element to change
	 * @param element The new element
	 * @return The element that was replaced
	 * @throws IndexOutOfBoundsException if the index is out of bounds.
	 */
	public E set(int index, E element) 
	{
		// TODO: Implement this method

        if (element == null)throw new NullPointerException("Data should not be null");
        if (index < 0 || index >= size())throw new IndexOutOfBoundsException("index if out of the List range");

        LLNode<E> cur = this.head;
        while (index-- > 0){
            cur = cur.next;
        }
        E data = cur.data;
        cur.data = element;
		return data;
	}

    @Override
    public String toString() {
        StringBuilder forward = new StringBuilder();
        StringBuilder reverse = new StringBuilder();
        LLNode<E> cur = this.head;
        while (cur != null){
            if (forward.length() > 0)forward.append(",");
            forward.append(cur.data);
            cur = cur.next;
        }
        cur = this.tail;
        while (cur != null){
            if (reverse.length() > 0)reverse.append(",");
            reverse.append(cur.data);
            cur = cur.prev;
        }

        return "Front"+ "{" + forward.toString() + "}\n"+
                "Back" + "{" + reverse.toString() + "}";
    }

    public void clear(){
        this.head = this.tail = null;
        return;
    }
}


class LLNode<E>
{
    LLNode<E> prev;
    LLNode<E> next;
    E data;

    // TODO: Add any other methods you think are useful here
    // E.g. you might want to add another constructor

    public LLNode(E e)
    {
        this.data = e;
        this.prev = null;
        this.next = null;
    }

}

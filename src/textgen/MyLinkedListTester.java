/**
 *
 */
package textgen;

import org.junit.Before;
import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * @author UC San Diego MOOC team
 *
 */
public class MyLinkedListTester {

	private static final int LONG_LIST_LENGTH =10;

	MyLinkedList<String> shortList;
	MyLinkedList<Integer> emptyList;
	MyLinkedList<Integer> longerList;
	MyLinkedList<Integer> list1;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		// Feel free to use these lists, or add your own
		shortList = new MyLinkedList<String>();
		shortList.add("A");
		shortList.add("B");
		emptyList = new MyLinkedList<Integer>();
		longerList = new MyLinkedList<Integer>();
		for (int i = 0; i < LONG_LIST_LENGTH; i++)
		{
			longerList.add(i);
		}
		list1 = new MyLinkedList<Integer>();
		list1.add(65);
		list1.add(21);
		list1.add(42);

	}


	/** Test if the get method is working correctly.
	 */
	/*You should not need to add much to this method.
	 * We provide it as an example of a thorough test. */
	@Test
	public void testGet()
	{
		//test empty list, get should throw an exception
		try {
			emptyList.get(0);
			fail("Check out of bounds");
		}
		catch (IndexOutOfBoundsException e) {

		}

		// test short list, first contents, then out of bounds
		assertEquals("Check first", "A", shortList.get(0));
		assertEquals("Check second", "B", shortList.get(1));

		try {
			shortList.get(-1);
			fail("Check out of bounds");
		}
		catch (IndexOutOfBoundsException e) {

		}
		try {
			shortList.get(2);
			fail("Check out of bounds");
		}
		catch (IndexOutOfBoundsException e) {

		}
		// test longer list contents
		for(int i = 0; i<LONG_LIST_LENGTH; i++ ) {
			assertEquals("Check "+i+ " element", (Integer)i, longerList.get(i));
		}

		// test off the end of the longer array
		try {
			longerList.get(-1);
			fail("Check out of bounds");
		}
		catch (IndexOutOfBoundsException e) {

		}
		try {
			longerList.get(LONG_LIST_LENGTH);
			fail("Check out of bounds");
		}
		catch (IndexOutOfBoundsException e) {
		}

	}


	/** Test removing an element from the list.
	 * We've included the example from the concept challenge.
	 * You will want to add more tests.  */
	@Test
	public void testRemove()
	{
		int a = list1.remove(0);
		assertEquals("Remove: check a is correct ", 65, a);
		assertEquals("Remove: check element 0 is correct ", (Integer)21, list1.get(0));
		assertEquals("Remove: check size is correct ", 2, list1.size());

		// TODO: Add more tests here

        MyLinkedList<Integer> temp = new MyLinkedList<>();
        temp.add(1);
        temp.add(2);
        temp.add(3);

        //Check head removal
        int b = temp.remove(0);
        assertEquals("Removing the first element of the list, is it Working?",1,b);
        assertEquals("Cheking the new head which should be next element of previous head", 2, temp.get(0).intValue());

        //Check tail removal
        b = temp.remove(temp.size() - 1);
        assertEquals("Removing the last element of the list, is it Working?",3,b);
        assertEquals("Cheking the new tail which should be previous element of tail", 2, temp.get(temp.size() - 1)
                .intValue
                        ());

        //Check removal when the size is 1
        b = temp.remove(0);
        assertEquals("Removing the lone element of the list, is it Working?",2,b);
        assertEquals("Is the list empty?",0,temp.size());

        //Restore the list
        temp.add(1);
        temp.add(2);
        temp.add(3);

        //Check removal not possible when index is not in range
        try{
            temp.remove(-1);
            fail("Its removing the unwanted index of List");
        }catch ( IndexOutOfBoundsException e){}

        try{
            temp.remove(temp.size());
            fail("Its removing the unwanted index of List");
        }catch (IndexOutOfBoundsException e){}

        try{
            temp.remove(temp.size() + 10);
            fail("Its removing the unwanted index of List");
        }catch (IndexOutOfBoundsException e){}



        //Check middle element removal
        b = temp.remove(1);
        assertEquals("Middle element should be same as removed element",2,b);
        assertEquals("Head should remain as it was before middle removed",1,temp.get(0).intValue());
        assertEquals("Tail should remain as it was before middle removed",3,temp.get(1).intValue());

        //Check removal until the list is empty and Checking whether its previous and next element is same or not
        // after deletion
        //Add 100 big Random element;
        Random random = new Random();
        for (int i = 4; i < 100; i++) {
            temp.add(random.nextInt());
        }

        while (temp.size() > 0){
            int del = random.nextInt(temp.size());
            int prev = -1,next = -1;
            if (del != 0)prev = temp.get(del - 1);
            if (del != temp.size() - 1)next = temp.get(del + 1);
            temp.remove(del);
            if (prev != -1)
                assertEquals("Left element is still same",prev, temp.get(del - 1).intValue());
            if (next != -1)
                assertEquals("Right element is still same", next, temp.get(del).intValue());
        }

	}

	/** Test adding an element into the end of the list, specifically
	 *  public boolean add(E element)
	 * */
	@Test
	public void testAddEnd()
	{
		// TODO: implement this test
        //Add 1 element and check the list is able to inset in the end
        list1.add(7);
        assertEquals("Check whether List is getting modifying or not", 7,list1.get(3).intValue());

        //Checking whether list is able to insert more than one element in the list or not
        list1.add(8);
        list1.add(9);
        list1.add(10);
        assertEquals("Check whether List is getting modifying or not", 8,list1.get(4).intValue());
        assertEquals("Check whether List is getting modifying or not", 9,list1.get(5).intValue());
        assertEquals("Check whether List is getting modifying or not", 10,list1.get(6).intValue());

        //Checking whether list is able to insert the element when the size is 0
        while (list1.size() != 0)list1.remove(0);
        //Temporary checking list becomes 0 or not
        assertEquals("Checking whether list is empty or not",0, list1.size());

        list1.add(1);
        assertEquals("Check whether List is adding in both head or tail", 1,list1.get(0).intValue());

        //check another adding in the end
        list1.add(2);
        list1.add(3);
        assertEquals("Check whether List has same head or not", 1,list1.get(0).intValue());
        assertEquals("Check whether List has middle or not", 2, list1.get(1).intValue());
        assertEquals("Check whether List has tail or not", 3,list1.get(2).intValue());

        //Checking addition of null elements
        try{
            list1.add(null);
            fail("Checking addition of null elements");

        }catch ( Exception e ){

        }

    }


	/** Test the size of the list */
	@Test
	public void testSize()
	{
		// TODO: implement this test
		assertEquals("Checking whether the size() method is working or not",3,list1.size());
		assertEquals("Checking whether the size() method is working or not",2,shortList.size());

        // now checking whether size method is changing after changes due to list modification operation

        // First add
        list1.add(5);
        assertEquals("List size should be changing after addition of one element",4, list1.size());

        //Now remove
        list1.remove(3);
        assertEquals("List size should get back to original after removal of last added element",3, list1.size());

        //Now return 0 in empty list

        list1.remove(0);
        list1.remove(0);
        list1.remove(0);
        assertEquals("List size should be 0", 0, list1.size());
	}



	/** Test adding an element into the list at a specified index,
	 * specifically:
	 * public void add(int index, E element)
	 * */
	@Test
	public void testAddAtIndex()
	{
		// TODO: implement this test

        MyLinkedList<Integer> temp = new MyLinkedList<>();
        //adding in an out of range index
        try{
            temp.add(1, 2);
            fail("Checking if it is able to insert out of Range");
        }catch ( Exception e ){

        }

        try{
            temp.add(-1, 2);
            fail("Check it it able to insert in negative index");
        }catch ( Exception e){}

        //adding in an empty list
        temp.add(0, 4);
        assertEquals("Checking insertion in an empty list", 4, temp.get(0).intValue());
        temp.add(1);

        //adding after head
        temp.add(1,3);
        assertEquals("Checking insertion just after head", 3, temp.get(1).intValue());

        //adding before tail
        temp.add(2,2);
        assertEquals("Checking insertion in the tail of list", 2, temp.get(2).intValue());

        //adding in the tail
        temp.add(temp.size(), 0);
        assertEquals("Checking insertion in the end", 0, temp.get(temp.size() - 1).intValue());

        //adding in the middle
        temp.add(3, 11);
        assertEquals("Checking insertion in the any particular index",11, temp.get(3).intValue());
	}

	/** Test setting an element in the list */
	@Test
	public void testSet()
	{
		// TODO: implement this test

        list1.clear();
        //Set the index which is not in range
        try{
            list1.set(0, 2);
            fail("Set the invalid index of list");

        }catch ( Exception e ){}

        //Reverse the list by using set function
        for (int i = 0; i < 10; i++) {
            list1.add(i + 1);
        }
        for (int i = 0; i < 10; i++) {
            list1.set(i, 10 - i);
        }
        for (int i = 0; i < 10; i++) {
            assertEquals("Setting of value is not working at particular index",10 - i, list1.get(i).intValue());
        }
    }


	// TODO: Optionally add more test methods.


//    @Test
//    public void testPrev() throws Exception {
////        list1.clear();
//        MyLinkedList<Integer> temp = new MyLinkedList<>();
//        int n = 10;
//        Random random = new Random();
//        int []arr = new int[n];
//        for (int i = 0; i < n; i++) {
//            arr[i] = random.nextInt(100000);
//        }
//        // for add method
//        for (int i = 0; i < n; i++) {
//            temp.add(arr[i]);
//        }
//        LLNode<Integer> tail = temp.tail;
//        for (int i = 0; i < n; i++) {
//            assertEquals("Check if previous pointers is working or not", arr[n - i - 1], tail.data.intValue());
//            tail = tail.prev;
//        }
//
//        //Testing after removing from last
//        while (temp.size() > 0){
//            Integer data = temp.tail.data;
//            temp.remove(temp.size() - 1);
//            assertEquals("Check after removal from end does change previous pointers", arr[temp.size],data.intValue() );
//        }
//    }
}

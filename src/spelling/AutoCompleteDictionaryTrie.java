package spelling;

//import sun.text.normalizer.Trie;

import java.util.List;
import java.util.Set;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;

/** 
 * An trie data structure that implements the Dictionary and the AutoComplete ADT
 * @author You
 *
 */
public class AutoCompleteDictionaryTrie implements  Dictionary, AutoComplete {

    private TrieNode root;
    private int size;
    

    public AutoCompleteDictionaryTrie() {
		root = new TrieNode();
	}

    public TrieNode getStem(String pref){
        pref = pref.trim();
        if (pref.length() == 0)
            return this.root;
        if(!pref.matches("[A-Za-z]+"))return null;
        TrieNode root = this.root.getChild(pref.charAt(0));
        for (int i = 1; i < pref.length(); i++) {
            if (root == null)return null;
            root = root.getChild(pref.charAt(i));
        }
        return root;
    }

    public boolean isStem(String pref){
        return getStem(pref) != null;
    }
	
	
	/** Insert a word into the trie.
	 * For the basic part of the assignment (part 2), you should ignore the word's case.
	 * That is, you should convert the string to all lower case as you insert it. */
	public boolean addWord(String word)
	{
	    //TODO: Implement this method.

        word = word.trim();
        if (word.equals(""))return true;
        word = word.toLowerCase();//Converting lower case

        TrieNode cur = root;
        boolean inserted = false;
        for (int i = 0; i < word.length(); i++) {
            TrieNode temp = cur.insert(word.charAt(i));
            if (temp != null){
                cur = temp;
            }
            else cur = cur.getChild(word.charAt(i));

            if (i == word.length() - 1){
                if (!cur.endsWord())inserted = true;
                cur.setEndsWord(true);
            }
        }
        size = inserted?1 + size:size;
        return inserted;
	}
	
	/** 
	 * Return the number of words in the dictionary.  This is NOT necessarily the same
	 * as the number of TrieNodes in the trie.
	 */
	public int size()
	{
	    //TODO: Implement this method
	    return size;
	}
	
	
	/** Returns whether the string is a word in the trie */
	@Override
	public boolean isWord(String s) 
	{
	    // TODO: Implement this method

        s = s.toLowerCase();//Converted to lower case
        TrieNode cur = root;
        for(char c:s.toCharArray()){
            if (cur.getChild(c) == null)return false;
            else cur = cur.getChild(c);
        }
        return cur != null && cur.endsWord()?true:false;
	}

	/** 
	 *  * Returns up to the n "best" predictions, including the word itself,
     * in terms of length
     * If this string is not in the trie, it returns null.
     * @param text The text to use at the word stem
     * @param n The maximum number of predictions desired.
     * @return A list containing the up to n best predictions
     */@Override
     public List<String> predictCompletions(String prefix, int numCompletions) 
     {
    	 // TODO: Implement this method
    	 // This method should implement the following algorithm:
    	 // 1. Find the stem in the trie.  If the stem does not appear in the trie, return an
    	 //    empty list
    	 // 2. Once the stem is found, perform a breadth first search to generate completions
    	 //    using the following algorithm:
    	 //    Create a queue (LinkedList) and add the node that completes the stem to the back
    	 //       of the list.
    	 //    Create a list of completions to return (initially empty)
    	 //    While the queue is not empty and you don't have enough completions:
    	 //       remove the first Node from the queue
    	 //       If it is a word, add it to the completions list
    	 //       Add all of its child nodes to the back of the queue
    	 // Return the list of completions
    	 List<String> completion = new LinkedList<>();

         //If prefix is in trie
         if (numCompletions == 0)return completion;
         if (prefix.length() != 0 && !isStem(prefix))return completion;

         List<TrieNode> queue = new LinkedList<>();
         TrieNode cur = getStem(prefix);
         queue.add(cur);
         if (isWord(cur.getText())){
             completion.add(cur.getText());
         }
//         DictionaryBST dictionary = new DictionaryBST();
         while (!queue.isEmpty()){
             TrieNode get = queue.remove(0);
             for (int i = 0; i < 26; i++) {
                 if (get.getChild((char)(97 + i)) != null){
                     TrieNode word = get.getChild((char)(97 + i));
                     if (isWord(word.getText())){
                        completion.add(word.getText());
                         if (completion.size() == numCompletions)return completion;
                     }
                     queue.add(word);
                 }
             }
         }

         return completion;
     }

 	// For debugging
 	public void printTree()
 	{
 		printNode(root);
 	}
 	
 	/** Do a pre-order traversal from this node down */
 	public void printNode(TrieNode curr)
 	{
 		if (curr == null) 
 			return;
 		
 		System.out.println(curr.getText()+" "+curr.endsWord());
 		
 		TrieNode next = null;
 		for (Character c : curr.getValidNextCharacters()) {
 			next = curr.getChild(c);
 			printNode(next);
 		}
 	}

    public static void main(String[] args) {
        AutoCompleteDictionaryTrie trie = new AutoCompleteDictionaryTrie();
        String []words = new String[]{"apple", "app","apltor","apstar", "my","word","a","app","a"};
        for(String word:words)trie.addWord(word);
        for(String k:trie.predictCompletions("",4)){
            System.out.println(k);
        }
        System.out.println(trie.size());

//        System.out.println(trie.isWord("app"));

    }


}